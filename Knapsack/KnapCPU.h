#include "pch.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <algorithm>

#include <omp.h>

int parse(const char* input, std::vector<int>& weights, std::vector<int>& values)
{
	// Init filestream
	std::fstream file(input, std::ios_base::in);

	// Read capacity
	int capacity = 0;
	file >> capacity;

	// Read next item and add to items
	int nextWeight, nextValue = 0;
	while (file >> nextWeight >> nextValue)
	{
		weights.push_back(nextWeight);
		values.push_back(nextValue);
	}

	return capacity;
}

#ifdef PC_THREADED
std::pair<int, int> knapsack(const int capacity, std::vector<int>& itemWeights, std::vector<int>& itemValues, const int numThreads)
#else
std::pair<int, int> knapsack(const int capacity, std::vector<int>& itemWeights, std::vector<int>& itemValues)
#endif
{
	auto start = std::chrono::steady_clock::now();

	int n = itemValues.size();
	std::vector<std::vector<int>> table(n + 1, std::vector<int>(capacity + 1, 0));

	for (int i = 0; i <= n; ++i)
	{
#ifdef PC_THREADED
		#pragma omp parallel for num_threads(numThreads)
#endif
		for (int w = 0; w <= capacity; ++w)
		{
			if (i == 0 || w == 0)
				table[i][w] = 0;
			else if (itemWeights[i - 1] <= w)
				table[i][w] = std::max(itemValues[i - 1] + table[i - 1][w - itemWeights[i - 1]], table[i - 1][w]);
			else
				table[i][w] = table[i - 1][w];
		}
	}

	int totalValue = table[n][capacity];
	int totalWeight = 0;
	
	int w = capacity;
	for (int i = n; i > 0 && totalValue > 0; i--) {

		// either the result comes from the top 
		// (table[i-1][w]) or from (itemValues[i-1] + table[i-1] 
		// [w-itemWeights[i-1]]) as in Knapsack table. If 
		// it comes from the latter one/ it means  
		// the item is included. 
		if (totalValue == table[i - 1][w])
			continue;
		else {

			// This item is included. 
			totalWeight += itemWeights[i - 1];

			// Since this weight is included its  
			// value is deducted 
			totalValue = totalValue - itemValues[i - 1];
			w = w - itemWeights[i - 1];
		}
	}

	auto end = std::chrono::steady_clock::now();

	std::cout << "Elapsed time (w/o file parsing): "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
		<< " ms" << std::endl << std::endl;

	return std::make_pair(totalWeight, table[n][capacity]);
}

void knapsack(char *argv[])
{
	// Parse arguments
	std::string localPath = std::string(argv[0]);
	std::string input = std::string(argv[1]);
	int numThreads = std::atoi(argv[2]);

	// Remvoe *.exe from localPath
	localPath = localPath.substr(0, localPath.find_last_of("\\") + 1);

	// Load problem from disk to memory
	std::vector<int> weights;
	std::vector<int> values;
	int capacity = parse((localPath + input).c_str(), weights, values);

	auto start = std::chrono::steady_clock::now();

#ifdef PC_THREADED
	auto solution = knapsack(capacity, weights, values, numThreads);
#else
	auto solution = knapsack(capacity, weights, values);
#endif

	std::cout << "Weight: " << solution.first << " / Value: " << solution.second << std::endl;
}