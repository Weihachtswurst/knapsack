#include "pch.h"
#include "../Knapsack/KnapCPU.h"
#include <iostream>

int main(int argc, char *argv[])
{
	if (argc != 3)
	{
		std::cout << "usage: Knapsack_Threaded.exe inputPath numOfThreads" << std::endl;
	}
	else
	{
		std::cout << "Processing using OpenMP" << std::endl;

		knapsack(argv);
	}
}