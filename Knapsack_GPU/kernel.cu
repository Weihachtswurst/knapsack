#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <chrono>
#include <vector>
#include <iostream>
#include <algorithm>
#include <fstream>

#include <stdio.h>

__global__ void blurEdge(int* table, int* itemWeights, int* itemValues, int i, int cap)
{
	int w = blockIdx.x * 512 + threadIdx.x;

	if (w < cap)
	{
		if (i == 0 || w == 0)
		{
			table[i * cap + w] = 0;
		}
		else if (itemWeights[i - 1] <= w)
		{
			int a = itemValues[i - 1] + table[(i - 1) * cap + (w - itemWeights[i - 1])];
			int b = table[(i - 1) * cap + w];

			table[i * cap + w] = a > b ? a : b;
		}
		else
		{
			table[i * cap + w] = table[(i - 1) * cap + w];
		}
	}
}


std::pair<int, int> knapsack(const int capacity, int* h_weights, int* h_values, int n)
{
	auto start = std::chrono::steady_clock::now();

	int m = capacity + 1;

	// Constants
	int size = (n + 1) * (capacity + 1);

	// Init table
	int* h_table = new int[size];
	for (int i = 0; i < size; ++i)
		h_table[i] = 0;

	// (1) allocate device memory
	int *d_table = 0, *d_weights = 0, *d_values = 0;
	cudaMalloc((void**)&d_table, size * 4);
	cudaMalloc((void**)&d_weights, n * 4);
	cudaMalloc((void**)&d_values, n * 4);

	// (2) transfer memory to GPU
	cudaMemcpy(d_table, h_table, size * 4, cudaMemcpyHostToDevice);
	cudaMemcpy(d_weights, h_weights, n * 4, cudaMemcpyHostToDevice);
	cudaMemcpy(d_values, h_values, n * 4, cudaMemcpyHostToDevice);

	int blocks = ((capacity + 1) / 512) + 1;

	for (int i = 0; i <= n; ++i)
	{
		blurEdge << <blocks, 512 >> >(d_table, d_weights, d_values, i, capacity + 1);
	}

	cudaDeviceSynchronize();

	// (4) transfer memory from GPU
	cudaMemcpy(h_table, d_table, size * 4, cudaMemcpyDeviceToHost);

	// (5) cleanup
	cudaFree(d_table);
	cudaFree(d_weights);
	cudaFree(d_values);

	// Post-Processing
	int totalValue = h_table[n * m + capacity];
	int totalWeight = 0;

	int w = capacity;
	for (int i = n; i > 0 && totalValue > 0; i--) {

		// either the result comes from the top 
		// (h_table[i-1][w]) or from (h_values[i-1] + h_table[i-1] 
		// [w-h_weights[i-1]]) as in Knapsack h_table. If 
		// it comes from the latter one/ it means  
		// the item is included. 
		if (totalValue == h_table[(i - 1) * m + w])
			continue;
		else {

			// This item is included. 
			totalWeight += h_weights[i - 1];

			// Since this weight is included its  
			// value is deducted 
			totalValue = totalValue - h_values[i - 1];
			w = w - h_weights[i - 1];
		}
	}

	auto end = std::chrono::steady_clock::now();

	std::cout << "Elapsed time (w/o file parsing): "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
		<< " ms" << std::endl << std::endl;

	return std::make_pair(totalWeight, h_table[n * m + capacity]);
}

int parse(const char* input, std::vector<int>& weights, std::vector<int>& values)
{
	// Init filestream
	std::fstream file(input, std::ios_base::in);

	// Read capacity
	int capacity = 0;
	file >> capacity;

	// Read next item and add to items
	int nextWeight, nextValue = 0;
	while (file >> nextWeight >> nextValue)
	{
		weights.push_back(nextWeight);
		values.push_back(nextValue);
	}

	return capacity;
}

int main(int argc, char *argv[])
{
	// Parse arguments
	std::string localPath = std::string(argv[0]);
	std::string input = std::string(argv[1]);
	int numThreads = std::atoi(argv[2]);

	// Remvoe *.exe from localPath
	localPath = localPath.substr(0, localPath.find_last_of("\\") + 1);

	std::vector<int> weights;
	std::vector<int> values;
	int capacity = parse((localPath + input).c_str(), weights, values);

	auto solution = knapsack(capacity, &weights[0], &values[0], values.size());

	std::cout << "Weight: " << solution.first << " / Value: " << solution.second << std::endl;
}