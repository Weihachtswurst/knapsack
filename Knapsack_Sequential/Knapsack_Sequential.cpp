// HorizontalImageBlur_Sequential.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "pch.h"
#include "../Knapsack/KnapCPU.h"
#include <iostream>

int main(int argc, char *argv[])
{
	if (argc != 3)
	{
		std::cout << "usage: Knapsack_Sequential.exe inputPath numOfThreads" << std::endl;
	}
	else
	{
		std::cout << "Processing using single thread" << std::endl;

		knapsack(argv);
	}
}
